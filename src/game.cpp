#include "game.hpp"
#include "state_machine.hpp"
#include "states/default_state.hpp"

Game::Game(int width, int height, const char *window_title, int framerate_limit,
           bool is_vsync)
    : _context(std::make_shared<GameData>()) {

  // Initialize window with specified config
  _context->window->create(sf::VideoMode(width, height), window_title,
                           sf::Style::Close | sf::Style::Titlebar);

  // Additional rendering configuration
  _context->window->setFramerateLimit(framerate_limit);
  _context->window->setVerticalSyncEnabled(is_vsync);

  // Add initial app state to boot into
  _context->state_machine->AddState(std::make_unique<DefaultState>(_context));

  // Start the loop
  this->Run();
}

Game::~Game() {}

// Game loop
void Game::Run() {
  sf::Clock clock;
  sf::Time time_since_last_frame = sf::Time::Zero;

  while (_context->window->isOpen()) {
    time_since_last_frame += clock.restart();

    while (time_since_last_frame > _delta_time) {
      time_since_last_frame -= _delta_time;

      // Execute all process in the state
      _context->state_machine->ProcessStateChanges();
      _context->state_machine->GetActiveState()->HandleInput();
      _context->state_machine->GetActiveState()->Update(_delta_time);
      _context->state_machine->GetActiveState()->Draw();
    }
  }
}
