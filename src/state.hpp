#pragma once

#include <SFML/System/Time.hpp>

namespace Seraphus {

/// State class that provides a structure of a member of a state machine.
/// This object is to be managed by the StateMachine.
class State {
public:
  State() {}
  virtual ~State() {}

  /// Called during state entry
  virtual void Initialize() = 0;

  /// Input-specific block is here
  virtual void HandleInput() = 0;

  /// Logic update here
  virtual void Update(sf::Time delta_time) = 0;

  /// Drawing update here
  virtual void Draw() = 0;

  virtual void Pause() = 0;
  virtual void Resume() = 0;
};
} // namespace Seraphus
