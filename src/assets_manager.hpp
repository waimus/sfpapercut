#pragma once

#include <map>
#include <memory>

#include <SFML/Graphics.hpp>
#include <SFML/Graphics/Font.hpp>

namespace Seraphus {

/// AssetManager provides tools and container to load and save external game
/// resource to the game runtime to be accessed by the game.
class AssetsManager {
public:
  AssetsManager() {}
  ~AssetsManager() {}

  /// Load texture asset and map them with the provided key string
  void LoadTexture(const char *name, const char *file_name,
                   bool is_repeated = false);

  /// Retrieve a texture asset from the fonts map by the provided key string
  sf::Texture &GetTexture(const char *name);

  /// Load font asset and map them with the provided key string
  void LoadFont(const char *name, const char *fileName);

  /// Retrieve a font asset from the fonts map by the provided key string
  sf::Font &GetFont(const char *name);

private:
  std::map<const char *, std::unique_ptr<sf::Texture>> _textures;
  std::map<const char *, std::unique_ptr<sf::Font>> _fonts;
};
} // namespace Seraphus
