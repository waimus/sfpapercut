#pragma once

#include <SFML/Graphics.hpp>

#include "../game.hpp"
#include "../state.hpp"

class SecondaryState : public Seraphus::State {
public:
  SecondaryState(GameDataRef data);
  ~SecondaryState();

  /// Called during state entry
  void Initialize() override;

  /// Input-specific block is here
  void HandleInput() override;

  /// Logic update here
  void Update(sf::Time delta_time) override;

  /// Drawing update here
  void Draw() override;

  void Pause() override {}
  void Resume() override {}

private:
  GameDataRef _context;

  sf::Text _game_title;
};
