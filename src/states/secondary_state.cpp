#include <iostream>

#include "secondary_state.hpp"
#include "default_state.hpp"
#include "../definitions.h"
#include "SFML/Graphics/Text.hpp"
#include "SFML/Window/Event.hpp"
#include "SFML/Window/Keyboard.hpp"

SecondaryState::SecondaryState(GameDataRef data) : _context(data) {}
SecondaryState::~SecondaryState() { _context.reset(); }

void SecondaryState::Initialize() {
  _context->window->setTitle("Current state: SecondaryState");

  _context->assets_manager->LoadFont("Patrick Hand SC REG",
                                     "../assets/ofl_patrick_hand_sc.ttf");
  _game_title.setFont(_context->assets_manager->GetFont("Patrick Hand SC REG"));
  _game_title.setString("This is another example state of the app");
  _game_title.setFillColor({255, 34, 68, 255});
  _game_title.setCharacterSize(64);
  _game_title.setOrigin(_game_title.getLocalBounds().width / 2,
                        _game_title.getLocalBounds().height / 2);
  _game_title.setPosition(_context->window->getSize().x / 2.0f,
                          _context->window->getSize().y / 2.0f);
}

void SecondaryState::HandleInput() {
  sf::Event event;
  while (_context->window->pollEvent(event)) {
    if (event.type == sf::Event::Closed) {
      _context->window->close();
    }

    if (event.type == sf::Event::KeyPressed) {
      if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
        _context->window->close();
      }

      if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1)) {
        _context->state_machine->AddState(
            std::make_unique<DefaultState>(_context), true);
      }

      if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2)) {
        std::cout << "You are already in SecondaryState\n";
      }
    }
  }
}

void SecondaryState::Update(sf::Time delta_time) {}

void SecondaryState::Draw() {
  _context->window->clear(BACKGROUND_COLOR);

  _context->window->draw(_game_title);

  _context->window->display();
}
