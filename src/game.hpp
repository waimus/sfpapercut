#pragma once

#include <memory>

#include "SFML/System/Time.hpp"
#include "assets_manager.hpp"
#include "input.hpp"
#include "state_machine.hpp"

/// GameData used as a container context of the current program instance.
struct GameData {
  std::unique_ptr<Seraphus::AssetsManager> assets_manager;
  std::unique_ptr<Seraphus::StateMachine> state_machine;
  std::unique_ptr<sf::RenderWindow> window;
  std::unique_ptr<Seraphus::InputManager> input_manager;

  GameData() {
    assets_manager = std::make_unique<Seraphus::AssetsManager>();
    state_machine = std::make_unique<Seraphus::StateMachine>();
    window = std::make_unique<sf::RenderWindow>();
    input_manager = std::make_unique<Seraphus::InputManager>();
  }
};

typedef std::shared_ptr<GameData> GameDataRef;

/// Entry class of the program
class Game {
public:
  Game(int width, int height, const char *window_title, int framerate_limit,
       bool is_vsync);
  ~Game();

private:
  GameDataRef _context = std::make_shared<GameData>();
  const sf::Time _delta_time = sf::seconds(1.0f / 60.0f);

  void Run();
};
