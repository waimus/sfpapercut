#include "state_machine.hpp"

namespace Seraphus {
StateMachine::StateMachine()
    : _is_adding(false), _is_removing(false), _is_replacing(false) {}

StateMachine::~StateMachine() {}

/// Add new state to the states stack, any changes will pe processed
/// automatically in the StateMachine::ProcessStateChanges()
void StateMachine::AddState(StateRef new_state, bool is_replacing) {
  this->_is_adding = true;
  this->_is_replacing = is_replacing;

  this->_newState = std::move(new_state);
}

void StateMachine::RemoveState() { this->_is_removing = true; }

void StateMachine::ProcessStateChanges() {
  // If needed to remove as long as the states stack is not empty
  // then allow removing.
  if (this->_is_removing && !this->_states.empty()) {
    this->_states.pop();

    // Run any state that is at the top of the stack if any.
    if (!this->_states.empty()) {
      this->_states.top()->Resume();
    }

    // Finish removing operation
    this->_is_removing = false;
  }

  // Adding operation
  if (this->_is_adding) {
    // If needed to replace as long as the states stack is not empty
    // Then remove the current state first
    if (this->_is_replacing && (!this->_states.empty())) {
      this->_states.pop();
      this->_is_replacing = false;
    }

    // Pause current state if any
    if (!this->_states.empty()) {
      this->_states.top()->Pause();
    }

    // Finally add new state to the stack
    this->_states.push(std::move(this->_newState));

    // Run the state at the top
    this->_states.top()->Initialize();
    this->_states.top()->Resume();

    // Finish adding operation
    this->_is_adding = false;
  }
}

StateRef &StateMachine::GetActiveState() { return this->_states.top(); }
} // namespace Seraphus
